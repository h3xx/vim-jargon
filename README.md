# vim-jargon

This is the project home for `syntax/jargon.vim` [included][syntax-file] in the [main Vim
distribution][vim-github].

## Jargon?

*Jargon* refers to Eric S. Raymond's `jargon.txt` project, a catalog of Unix
hacker terminology and slang. The main project page can be found on [Eric's
site][catb].

This syntax file is specifically for the [text version of the Jargon
collection][catb-text-files].

## Installation

**This already came with your copy of Vim**, but I guess you want the latest
bleeding-edge version:

Vim 8+:

```sh
mkdir -p ~/.vim/pack/dist/start
git clone https://codeberg.org/h3xx/vim-jargon.git ~/.vim/pack/dist/start/vim-jargon
```

Vim 7 and older:

```sh
mkdir -p ~/.vim/syntax
wget -O ~/.vim/syntax/jargon.vim https://codeberg.org/h3xx/vim-jargon/raw/branch/main/syntax/jargon.vim
```

[catb-text-files]: https://www.catb.org/jargon/oldversions/?C=M;O=D
[catb]: https://www.catb.org/jargon/
[syntax-file]: https://github.com/vim/vim/blob/master/runtime/syntax/jargon.vim
[vim-github]: https://github.com/vim/vim
