" XXX You don't need these lines in $VIMRUNTIME/filetype.vim or
" $VIMRUNTIME/ftdetect/; it's already included in Vim.
" This is only here for a reference of how jargon.vim gets loaded.

" --- vim v7.0001 (2004-06-31) ---
" Jargon
au! BufNewFile,BufRead *jarg*
	\ if getline(1).getline(2).getline(3).getline(4).getline(5) =~? 'THIS IS THE JARGON FILE' |
	\   setf jargon |
	\ endif
