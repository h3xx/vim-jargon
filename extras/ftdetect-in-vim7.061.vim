" XXX You don't need these lines in $VIMRUNTIME/filetype.vim or
" $VIMRUNTIME/ftdetect/; it's already included in Vim.
" This is only here for a reference of how jargon.vim gets loaded.

" --- vim v7.0061 (2005-03-18) ---
" Function used for patterns that end in a star: don't set the filetype if the
" file name matches ft_ignore_pat.
" When using this, the entry should probably be further down below with the
" other StarSetf() calls.
func s:StarSetf(ft)
  if expand("<amatch>") !~ g:ft_ignore_pat
    exe 'setf ' . a:ft
  endif
endfunc

" Jargon
au! BufNewFile,BufRead *jarg*
	\ if getline(1).getline(2).getline(3).getline(4).getline(5) =~? 'THIS IS THE JARGON FILE'
	\|  call s:StarSetf('jargon')
	\|endif
